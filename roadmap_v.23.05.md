# Draft: Roadmap v.23.05 Release

Draft based on WG retrospective and planning session ([outcome](https://community.gaia-x.eu/apps/files/?dir=/Policy%20Rules%20Committee/WG%20Compliance/Meeting%20minutes/20221122&fileid=15904663)) on 22th November 2022

**Additional feedback and inputs are welcome.**

## General conditions (way of working) 
- **Timeline**: 
   - The next stable release will be on 23.05.2023. 
   - The release has to be finalised ~3-4 Weeks before to ensure enough time for the BoD review and agreement.
   - Stable (software) milestones might be released at a higher frequency e.g. for hackathons.
- **Communication**:
  - Better and more detailed alignment with other WGs (a starting point could be a PRC roadmap)
  - Linking with the technical part and the federation services needs to be stronger
  - Validation of the relevance of the attributes of the lighthouse projects

## Priorities of the deliverables (ordered descending based on member votes)

- for every Label criteria, define measurable and comparable [low level requirements (LLR)](https://openecu.com/low-level-requirements-software/)
(translation from those LLR to attributes will be done under the TC)

- Concrete description on being **Gaia-X Compliant** (=compliance with the Trust Framework) based on the role

- More mature **data protection** section

- Attributes/Controls for **interoperability, portability, switchability and IP-Protection**

- **Trust anchors**, legal validation and concrete process

- **Clear wording**, definitions and distinctions of functions within the ecosystem 
(e.g. Notary, CAB, Trust Anchor ...) -->potentially solvable with the Gaia-X glossary 


## Additional points (based on the last roadmap)

- **Transparency on environmental impact**: measurements have to be defined (highly complex to measure on service / customer instance level)
- **(Participant)/natural person**: More comprehensive description, additional attributes needed. Currently marked as experimental within the Trust Framework


