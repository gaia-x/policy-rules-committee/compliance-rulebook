# Participant

A Participant is a Legal Person or Natural Person, which is identified, onboarded and has a Gaia-X Self-Description. Instances of a Participant neither being a legal nor a natural person are prohibited.

```mermaid
classDiagram

class Participant {
    <<abstract>>
}

Participant <|-- LegalPerson
Participant <|-- NaturalPerson
```

The Architecture Document defines three roles a Participant can have within the Gaia-X Ecosystem (Provider, Consumer, and Federator). These are not yet part of Trust Framework and are to be defined in future releases.

## Issuer

Each issuer shall issue a `GaiaXTermsAndCondition` verifiable credential as follow:

| Attribute                                                           | Cardinality | Trust Anchor | Comment                                           |
|---------------------------------------------------------------------|------|-----------|---------------------------------------------------|
| `termsAndConditions`                                                | 1    | `issuer` | SHA512 of the Generic Terms and Conditions for Gaia-X Ecosystem as defined below |

<details>
<summary> Example of T&C signed by the issuer</summary>

```json
{
  "id": "did:example:issuer/tandc",
  "type": [
    "VerifiableCredential"
  ],
  "issuer": "did:example:issuer",
  "issuanceDate": "2022-06-12T19:38:26.853Z",
  "credentialSubject": [
    {
      "@id": "did:example:issuer/tandc/1",
      "type": "GaiaXTermsAndCondition",
      "termsAndConditions": {
        "@value": "0f5ced733003d11798006639a5200db78206e43c85aa123456789789123456798",
        "@type": "xsd:string",
        "@checksumtype": "SHA-256"
      }
    }
  ],
  "proof": {
    "type": "JsonWebSignature2020",
    "verificationMethod": "did:example:issuer#key",
    "created": "2022-06-12T19:38:26.853Z",
    "proofPurpose": "assertionMethod",
    "jws": "z2iiwEyyGcqwLPMQDXnjEdQU4zGzWs6cgjrmXAM4LRcfXni1PpZ44EBuU6o2EnkXr4uNMVJcMbaYTLBg3WYLbev3S"
  }
}
```

</details>

## Legal person

For legal person the attributes are

| Attribute                                                           | Cardinality | Trust Anchor | Comment                                           |
|---------------------------------------------------------------------|------|-----------|---------------------------------------------------|
| `registrationNumber`                                                | 1    | registrationNumberIssuer | Country's registration number, which identifies one specific entity. |
| `headquartersAddress`.[`countryCode`](https://schema.org/addressCountry) | 1    | State | Physical location of the headquarters in [ISO 3166-2](https://www.iso.org/standard/72483.html) alpha2, alpha-3 or numeric format. |
| `legalAddress`.[`countryCode`](https://schema.org/addressCountry)       | 1    | State | Physical location of legal registration in [ISO 3166-2](https://www.iso.org/standard/72483.html) alpha2, alpha-3 or numeric format. |
| [`parentOrganization[]`](https://schema.org/parentOrganization)     | 0..* | State | A list of direct `participant` that this entity is a subOrganization of, if any. |
| [`subOrganization[]`](https://schema.org/subOrganization)           | 0..* | State | A list of direct `participant` with a legal mandate on this entity, e.g., as a subsidiary. |

### registrationNumber

The list of valid entity `registrationNumber` type are described below:

| Attribute | Comment                                           |
|-----------|---------------------------------------------------|
| [`local`](https://schema.org/taxID)   | the state issued company number                   |
| [`EUID`](https://schema.org/Text)    | the [European Unique Identifier (EUID)](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32020R2244&from=en#d1e1428-3-1) for business located in the [European Economic Area](https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Glossary:European_Economic_Area_(EEA)), Iceland, Liechtenstein or Norway and registered in the Business Registers Interconnection System ([BRIS](https://e-justice.europa.eu/content_business_registers_at_european_level-105-en.do)). This number can be found via the [EU Business registers portal](https://e-justice.europa.eu/content_find_a_company-489-en.do) |
| [`EORI`](https://schema.org/Text)    | the [Economic Operators Registration and Identification number (EORI)](https://ec.europa.eu/taxation_customs/business/customs-procedures-import-and-export/customs-procedures/economic-operators-registration-and-identification-number-eori_en). |
| [`vatID`](https://schema.org/vatID)   | the VAT identification number. |
| [`leiCode`](https://schema.org/leiCode) | Unique LEI number as defined by <https://www.gleif.org>. |

**Consistency rules**

- if several numbers are provided, the information provided by each number must be consistent.

### Gaia-X Ecosystem Terms and Conditions

```text
The PARTICIPANT signing Gaia-X credentials agrees as follows:
- to update its Gaia-X credentials about any changes, be it technical, organizational, or legal - especially but not limited to contractual in regards to the indicated attributes present in the Gaia-X credentials.

The keypair used to sign Gaia-X credentials will be revoked where Gaia-X Association becomes aware of any inaccurate statements in regards to the claims which result in a non-compliance with the Trust Framework and policy rules defined in the Policy Rules and Labelling Document (PRLD).
```

## Validation of a Natural person (Experimental)

The Functional Requirement to validate if a remote interaction is done by a natural person is of interest for several usecases in Gaia-X; consent management, rights delegation, data & service contract negotiation, ...

The proposal below is to bound the verification of an interaction with a natural person to an action on a physical device. The device shall provide a mean to remotely validate its physical and software integrity.  
The list of technical solution being investigated are:

- [WebAuthn](https://www.w3.org/TR/webauthn-3/) with FIDO2 dongles
    - a non exhaustive list can be found here <https://www.dongleauth.com/dongles/>
    - [demo](https://demo.yubico.com/webauthn-technical/) with [Yubikey](https://www.yubico.com/products/)
        - Root CA validation can be made from <https://developers.yubico.com/PIV/Introduction/PIV_attestation.html>
- Android applications using [Google Play Integrity API](https://developer.android.com/google/play/integrity/overview) with a nonce given by the Verifier.
    - the returned value of [`IntegrityTokenResponse.token()`](https://developer.android.com/google/play/integrity/verdict#decrypt-verify-locally) must be shared with the Verifier.
    - Root CA validation can be made from <https://www.googleapis.com/oauth2/v1/certs>
- Push notification to an application providing workload remote attestation[^remoteattestation] supported by TPM2.0 modules.
    - Root CA validation can be made by providing the [Endorsement Key certificate (EKCert)](https://trustedcomputinggroup.org/resource/tcg-ek-credential-profile-for-tpm-family-2-0/) from the TPM module.

[^remoteattestation]: <https://ec.europa.eu/research/participants/documents/downloadPublic?documentIds=080166e5cfd3eb3b&appId=PPGMS>

To be noted that in the above context, public keys do not uniquely identify natural persons and hence are not considered as PII.

<!--
| Attribute                                                           | Card.| Trust Anchor | Comment                                           |
|---------------------------------------------------------------------|------|--------------|---------------------------------------------------|
| [`contactPoint[]`](https://schema.org/ContactPoint)                 | 1..* | SSI          | point of contact, such as an email                |
| [`proofOfResidence[]`](https://schema.org/Residence)                | 1..* | SSI          | list of proof of residence from which a participant can request the `countryCode` |
-->

